var net = require("net"), 
	app = require('http').createServer(handler), 
	io  = require('socket.io').listen(app), 
	fs  = require('fs');


var port = (process.argv.length == 3) ? Number( process.argv[2] ) : 8080;

app.listen(port);


function handler (req, res) {

	console.log(req.url);

	if (req.url.match(/([\.])/g)) {

		fs.readFile(__dirname + req.url,
			function (err, data) {

				if (err) {

					res.writeHead(500);
					return res.end('Error loading' + req.url);
				}

				//res.setHeader( 'Content-Type', 'text/html;charset=UTF-8' );
				res.writeHead(200);
				res.end(data);
			}
		);
	}
	else {

		fs.readFile(__dirname + '/irc_client.html',
			function (err, data) {

				if (err) {

					res.writeHead(500);
					return res.end('Error loading index.html');
				}

				res.setHeader( 'Content-Type', 'text/html;charset=UTF-8' );
				res.writeHead(200);
				res.end(data);
			}
		);
	}
}


io.sockets.on('connection', function (webSocket) {

	var tcpStream = new net.Stream;
	
	tcpStream.setTimeout(0);
	tcpStream.setEncoding("ascii");
	
	webSocket.on('message', function (message) {

		try {

			if (message.split(' ')[0] == 'CONNECT') {
				
				var host = message.split(' ')[1].split(':')[0];
				var port = message.split(' ')[1].split(':')[1];
				
				console.log( 'connecting to '+host+':'+port+'…' );
				
				tcpStream.destroy();
				tcpStream.connect( port, host );
			}

			else {

				tcpStream.write(message);
			}
		}
		catch (e) {

			webSocket.send(e);
		}
	});
    
	webSocket.on('disconnect', function () {

		tcpStream.end();
	});

	tcpStream.addListener("error", function (error) {

		webSocket.send(error+"\r\n");
	});
    
	tcpStream.addListener("data", function (data) {

		webSocket.send(data);
	});
	
	tcpStream.addListener("close", function () {

		webSocket.send("Servidor encerrou a conexao. Voce esta offline. Use /connect para conectar.");
	});
});