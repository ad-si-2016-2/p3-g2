/* Funções principais do chat IRC Web com Socket.io */


var current_channel = 'server';                 // Nome da conversação selecionada
var socket = io.connect(window.location.host);  // Instância do web socket
var nick = new DOM_Element ('#nick');           // Elemento nickname indexado no HTML

// Inicialização da página
$(document).ready( function () {

	response.socket = socket;

	// Trata o clique do botão no formulário de conexão inicial
	$("#connect-btn").click( function (e) {

		e.preventDefault();
		
		irc_connect();
	});

	// Trata o envio de um texto ao chat irc
	$('#bottom-box form').submit( function(e) {

		e.preventDefault();

		var msg = $('#msginput').val();

		$('#msginput').val('');

		send_message(msg);
	});
});


function send_message (msg) {

	if(msg.match(/^\//)){

		// Formata mensagem
		msg = msg.replace(/^\//, '');
		command = msg.split(' ')[0].toUpperCase();
		msg = msg.replace(/^([a-zA-Z]+)/, command);
		msg += "\r\n";


		if (command == "CONNECT") {

			socket.send(msg);
			
			socket.send("NICK "+ nick.val() +"\r\n");
			
			socket.send("USER "+ nick.val() + ' '
				+ $("#server").val().split(":")[0] 
				+ " blubb :"+ nick.val() +"\r\n");
		} 

		else if ((command == "PART")&&(!msg.split(' ')[1])) {

			// no channel given -> use current channel
			socket.send('PART ' + current_channel + "\r\n" ); 
		} 

		else if (command == "QUERY") {

			var username = msg.split(' ')[1];

			if ($('#'+username).length == 0) {

				switch_channel( username );

				$('#channel').append( '<li onclick="switch_channel($(this).text() );" id="' 
					+ username + '">' + username + '</li>' );

				showMsg('Private conversation with '+username, current_channel);
			}
		} 

		else if (command == "MSG") {

			var username = msg.split(' ')[1];
			var message = msg.substr(msg.substr(5).indexOf(' ')+6);
			
			socket.send("PRIVMSG "+username+" :"+message+"\r\n");
			
			showMsg(nick.val() + " -> "+ username +" : " + message, "server");
			showMsg(nick.val() + " : " + message, username);
		}

		else {

			// Envia comando não registrado
			socket.send(msg + "\r\n");
		}
	}
	
	else {

		socket.send("PRIVMSG "+current_channel+" :"+msg+"\r\n");
		showMsg(nick.val() +" : " + msg, current_channel);
	}
}


function irc_connect () {

	$("#connect").hide();
	$("#ui").show();
	$("#msginput").focus();

	socket.send("CONNECT "+ $("#server").val());
	socket.send("NICK "+ nick.val()  +"\n");
	socket.send("USER "+ nick.val()  + ' '
		+ $("#server").val().split(":")[0] 
		+ " blubb :"+nick.val()  +"\n");
}


function timestamp () {

	var date   = new Date();
	var hour   = date.getHours();
	var minute = date.getMinutes();
	
	hour   = (hour<10)?   '0'+hour : hour;
	minute = (minute<10)? '0'+minute : minute;
	return hour+":"+minute;
}


function showMsg (msg, channel){

	msg = "["+ timestamp() + "] " + msg;

	msg = $('<div/>').text(msg).html();  
	
	$("#messages").append( '<li '
			+ ( ( channel != current_channel ) ? 'style="display:none;"' : '' ) +
			' class="' + channel.replace( /^#/, '' ) + '">' + msg + "</li>").ready(function(){ 
		var scrollbox = document.getElementById('messages');
		scrollbox.scrollTop = scrollbox.scrollHeight;
	});
	
	$("#top-box").scrollTop($("#messages").height());
}


function switch_channel (c) {

	$('#messages li').hide();
	$('li.' + c.replace( /^#/, '' ) ).show();
	current_channel = c;

	$("#top-box").scrollTop($("#messages").height());
	$("#msginput").focus();
}


function parse_msg (msg) {

	var message = [];

	// Primeira checagem (Server)
	// [1]:server [2]:message
	if(args = msg.match(/^:([\w.]+) (.*)/)){
		message["sender"] = "server";
		message["server"] = args[1];
		content = args[2];
	}
	// Primeira checagem (User)
	// [1]:Nick [2]:User [3]:host [4]:message
	else if(args = msg.match(/^:(.*)!(.*)@([\w.\/\\:]+) (.*)/)){
		message["sender"] = "user";
		message["nick"] = args[1];
		message["user"] = args[2];
		message["host"] = args[3];
		content = args[4];
	}
	//Fora do padrão do protocolo
	else{
		return false;
	}
	// Segunda checagem (Reply)
	// [1]:code [2]:nick [3]:message
	if(args = content.match(/^([\d]+) ([\w\-_*#!&+]+) (.*)/)){
		message["type"] = "reply";
		message["id"] = args[1];
		message["receiver"] = args[2];
		message["content"] = args[3].replace(/^:/, '');
	}
	// Segunda checagem (Command)
	// [1]:command [2]:nick [3]:message
	else if(args = content.match(/^([\w]+) (.*)/)){
		message["type"] = "command";
		message["id"] = args[1].toUpperCase();
		message["content"] = args[2];
		if(args = message["content"].match(/^([\w\-_*#!&+]+) (.*)/)){
			message["receiver"] = args[1];
			message["content"]  = args[2].replace(/^:/, '');
		}
		else if(args = message["content"].match(/^:(.*)/)){
			message["content"]  = args[1].replace(/^:/, '');
		}
		else if(args = message["content"].match(/^([\w\-_*#!&+]+)/)){
			message["receiver"] = args[1];
		}
	}
	//Fora do padrão do protocolo
	else{
		return false;
	}

	return message;
}

socket.on('message', function (data) {

	var lines = data.split( "\r\n" );
	
	for ( var i = 0; i < lines.length; i++ )  {

		var line = parse_msg(lines[i]);

		console.log(line);

		if (line.type == "command") {

			if (response[line.id]) response[line.id](line);
		}
		
		else if (line.type == "reply") {

			if (response["R" + line.id]) response["R" + line.id](line);
		}


		if (line) {

			showMsg(lines[i], 'server');
		}
	}
});