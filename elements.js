
function DOM_Element (selector) {
	
	this.selector = selector;

	this.get = function () {

		return $(this.selector);
	};

	this.val = function (value) {
		
		if (!value) {

			return ($(this.selector).val())? $(this.selector).val() : $(this.selector).html();
		}
		else {

			$(this.selector).html(value);

			if ($(this.selector).attr("value")) $(this.selector).val(value);
		}
	};

	this.attr = function (name, value) {

		if (!value) {

			return $(this.selector).attr(name);
		}
		else {

			this.attr(name, value);
		}
	};
}