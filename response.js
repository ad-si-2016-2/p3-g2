
var response = {
	
	socket: null, 


	/* TRATAMENTO DE COMANDOS */
	
	NICK: function (msg) {
		
		// Se cliente mudou o próprio nick
		if (msg.nick == nick.val()) {

			nick.val(msg.content);
		}

		// Exibe notificação
		showMsg(msg.nick + " mudou o nome para " + msg.content, "server");
	}, 

	PING: function (msg) {

		this.socket.send( 'PONG ' + msg.content + '\r\n' );
		console.log("Replying with PONG");
	}, 

	PRIVMSG: function (msg) {

		if ($('#'+msg.receiver.replace(/^#/, '')).length == 0) {

			$('#channel').append( '<li onclick="switch_channel($(this).text());" id="' 
			+ msg.receiver + '">' + msg.receiver + '</li>' );
		}

		showMsg(msg.nick + ' : ' + msg.content, msg.receiver);
	}, 

	JOIN: function (msg) {
		
		if (msg.nick == $('#nick').val()) {

			switch_channel(msg.receiver);

			$('#channel').append( '<li onclick="switch_channel($(this).text());" id="' 
				+ msg.receiver.replace( /^#/, '' ) + '">' + msg.receiver + '</li>' );
		}
	}, 

	PART: function (msg) {
		
		if (msg.receiver == $('#nick').val()) {

			switch_channel('server');
			$(msg.content).remove();
		}
	}, 

	MODE: function (msg) {
		
		showMsg('Mode by ' + msg.content, msg.receiver);
	}, 


	/* RESPOSTAS NUMÉRICAS */

	R332: function (msg) {

		showMsg(msg.content, msg.receiver);
	}, 

	R353: function (msg) {

		showMsg('User in channel: ' + msg.content, msg.receiver);
	}
}